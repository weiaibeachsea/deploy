# 本地根链部署文档

该文档主要说明本地根链的部署目标和具体的部署步骤。

[TOC]

## 部署目标

在该本地根链中，一共存在5个组织。分别为hit组织，pcnl组织，ncse组织，iana组织和orderer组织，其中，hit组织，pcnl组织，ncse组织，iana组织为peer组织，每个peer组织包含1个peer节点；orderer组织包含1个orderer节点。

一个组织，对应一台物理机。

网络的具体结构和配置信息如下所示：

```YAML
---
ordererOrgs：
  - &orderer:
    name: orderer
    nodes:
	  - &orderer0:
	    name: orderer0.orderer
	    host: 10.10.11.11
	    port: 11050
	ca:
	  name: ca.orderer
	  host: 10.10.11.11
	  port: 11054
	  
peerOrgs:
  - &cn:
    name: cn
    nodes:
	  - &peer0:
	    name: peer0.cn
	    host: 10.10.11.31
	    port: 7050
    ca:
      name: ca.cn
      host: 10.10.11.31
      port: 7054
  - &ru:
    name: ru
    nodes:
	  - &peer0:
	    name: peer0.ru
	    host: 10.10.11.130
	    port: 8050
    ca:
      name: ca.ru
      host: 10.10.11.130
      port: 8054
  - &iana:
    name: iana
    nodes:
	  - &peer0:
	    name: peer0.iana
	    host: 10.10.11.131
	    port: 9050
    ca:
      name: ca.iana
      host: 10.10.11.131
      port: 9054
```

演示程序的ip,端口如下。这些ip,端口会出现在命令行或配置文件中，注意根据实际情况修改。

| 节点             | 所在ip                | port  |
| ---------------- | --------------------- | ----- |
| orderer0.orderer | 10.10.11.11（主机1）  | 11050 |
| peer0.hit        | 10.10.11.31（主机2）  | 7050  |
| peer0.pcnl       | 10.10.11.130（主机3） | 8050  |
| peer0.iana       | 10.10.11.131（主机4） | 9050  |
| peer0.ncse       | 10.10.11.131（主机5） | 10050 |




## 部署需求

### 系统要求

- docker v18.06及以上
- docker-compose v1.23及以上
- Fabric v2.2.0
- Fabric CA v1.4.8

### 预处理步骤

- 将 [localrootchain项目组](https://gitlab.com/localrootchain)中的所有项目下载到本地同一目录下。
- 将 [fabric v2.2.0程序](https://github.com/hyperledger/fabric/releases/download/v2.2.0/hyperledger-fabric-linux-amd64-2.2.0.tar.gz)下载到本地并解压到localrootchain项目组目录下。
- 将 fabric v2.2.0镜像和fabric-ca v1.4.8镜像下载到本地。



## 准备工作：

1. 在每台主机上修改project/script目录下的 `env-var.sh `中的allOrg变量的值,赋值为整个网络的所有组织的组织名。

   示例网络有5个组织，分别是orderer,hit,pcnl,ncse,iana，则修改方法如下：

   allOrg=("orderer" "hit" "pcnl" "iana" "ncse")，注,orderer组织必须是数组第一个元素，放最前。

2. 在每台主机上修改project/script目录下的 `env-var.sh `中的orgname变量的值, 改为本机负责部署的组织的集合。

   举例，欲在主机1上部署orderer组织，则在主机1的script目录下的env-var.sh脚本中，把orgname赋值为orderer，即orgname=("orderer")

   举例，欲在主机4上部署iana组织，则在主机4的script目录下的env-var.sh脚本中，把orgname赋值为iana，即orgname=("iana")

   举例，欲在主机3上部署hit和orderer组织，则在主机3的script目录下的env-var.sh脚本中，把orgname赋值为hit和orderer,即orgname=("hit" "orderer")

3. 修改projectPath变量，改为项目根目录的绝对路径，示例：projectPath="/home/deploy/my-awesome-project/",需保证各主机的项目路径相同，否则需要修改scp命令中的路径

4. 在每台主机上，修改orgxIP变量，赋值为对应的ip地址。

   示例，假如在第一步中，allOrg被修改为：allOrg=("orderer" "hit" "pcnl" "iana" "ncse")

   则修改org1IP为orderer组织的IP，修改org2IP为hit组织的IP，修改org3IP为pcnl组织的IP，修改org4IP为iana组织的IP，修改org5IP为ncse组织的ip

5. 在各个主机上，根据实际情况，修改各组织目录下的`fabric-ca-server-config.yaml`配置文件中的ip地址；

   （比如：在主机1上部署orderer组织的节点，则在主机1的organization/orderer/fabric-ca-server-config.yaml中，把hosts的ip，修改为主机1的ip；

   主机2部署cn节点，则在主机2的organization/cn/fabric-ca-server-config.yaml中，把hosts的ip，修改为主机2的ip；

   主机3，4以此类推）

6. 在各个主机上，修改project/configtx.yaml文件，根据自身实际情况，修改配置文件中个组织的锚节点的ip



## 自动部署脚本

#### 第一次部署：

1. 所有主机，于scripts目录下执行`./onekeyrestart-step1.sh -a`

   -a，即"all"，意味着从0开始，包括ca节点的部署

2. orderer所在主机，于scripts目录下执行`./onekeyrestart-mainnode-step2.sh -a`，期间需要输入密码(文件传输)

   -a，即"all"，意味着从0开始，包括证书的获取

3. 等待上一步完全结束之后，于除orderer以外的每个主机上，scripts目录下

   执行`./onekeyrestart-othernode-step2.sh -a`

   -a，即"all"，意味着从0开始，包括证书的获取

4. 等待上一步完全结束之后，于cn所在主机上，scripts目录下执行`./commit-chaincode.sh`



#### 重启网络：

适用于不改变组织的位置，只重启网络

1. 所有主机，于scripts目录下执行`./onekeyrestart-step1.sh`

2. orderer所在主机，于scripts目录下执行`./onekeyrestart-mainnode-step2.sh`

3. 等待上一步完全结束之后，于除orderer以外的每个主机上，scripts目录下

   执行`./onekeyrestart-othernode-step2.sh`

4. 等待上一步完全结束之后，于cn所在主机上，scripts目录下执行`./commit-chaincode.sh`







## 手动部署

本地根链手动部署主要流程如下：

-  创建各个组织的Fabric CA
-  在Fabric CA上为组织成员注册并发放证书
-  编辑网络配置文件，生成建立区块链网络必需的文件
-  创建Fabric网络中定义的节点
-  创建通道，将节点加入通道
-  部署链码
-  连接客户端

### 1. 创建各个组织的Fabric CA

在各机器上的script目录中，执行`set-org-ca.sh`    ，以在不同机器上，分别启动各组织fabric ca。




过程分析：

所有组织启动CA的方式完全相同，以下以cn组织为例说明如何启动cn组织的fabric-ca节点。

首先进入`organization/cn`目录下，在目录下存在`docker-compose-ca.yaml`，`.env`和`fabric-ca-server-config.yaml`配置文件，其中`.env`配置如下：

```
COMPOSE_PROJECT_NAME=net
FABRIC_CA_IMAGE_TAG=1.4.8
FABRIC_IMAGE_TAG=2.2.0
ORG_NAME=cn
FABRIC_CA_PORT=7054
PEER0_PORT=7050
COUCHDB_PEER0_PORT=7984
```

配置文件`docker-compose-ca.yaml`的内容如下所示：

```YAML
version: '3.2'

networks:
  localrootchain:

services:
  ca.cn:
    image: hyperledger/fabric-ca:$FABRIC_CA_IMAGE_TAG
    container_name: ca.$ORG_NAME
    environment:
      - FABRIC_CA_HOME=/etc/hyperledger/fabric-ca-server
      - FABRIC_CA_SERVER_CA_NAME=ca.$ORG_NAME
      - FABRIC_CA_SERVER_PORT=7054
    ports:
      - "$FABRIC_CA_PORT:7054"
    command: sh -c 'fabric-ca-server start -b admin:adminpw -d' # 此处给出了Fabric CA初始成员的登录信息，发放证书时会用到
    volumes:
      - ./ca-home/:/etc/hyperledger/fabric-ca-server
    networks:
      - localrootchain
```

获取默认的`fabric-ca-server-config.yaml`配置文件，<span style='color:red;background:white;font-size:20px;font-family:微软雅黑;'>(注意根据实际情况，对ip地址进行修改)</span>，并修改CA证书注册请求如下：

```YAML
csr:
   cn: ca.cn
   names:
      - C:
        ST:
        L:
        O: localrootchain
        OU:
   hosts:
     - cn
     - 10.10.11.31（待修改）
   ca:
      expiry: 131400h
      pathlength: 1
```

在完成以上配置文件的修改后，执行如下命令，启动Fabric CA。

```shell
rm -r ca-home/*
cp fabric-ca-server-config.yaml ca-home
docker-compose -f docker-compose-ca.yaml up -d
```

其他的组织启动Fabric CA的方式基本相同，只需根据实际修改组织名和CA服务提供的地址即可。

### 2. 在Fabric CA上为组织成员注册并发放证书

1. 首先在所有节点，执行 project/script 目录下的 `pull-tls-pem.sh`，该脚本包含scp命令，（注意根据实际情况情况。对脚本中的路径进行修改）会拉取各个节点的tls证书，不这样做的话，后面的步骤无法进行。

2. 在所有主机，运行script目录下的`get-cert.sh`，该命令将会生成所有组织的所有成员的MSP和部分成员的TLS连接信息。




过程分析：

Fabric作为许可链，每个成员必须有被Fabric网络中其他成员承认的证书才能与其他成员交互。在Fabric中，每个成员的身份信息通过MSP来管理，同时，部分成员之间的通信采用了TLS协议，因此这些成员同时需要TLS证书，以下步骤以身份peer0.cn为例，说明如何获取某个成员的MSP信息和TLS连接信息。

#### (1) 获取初始成员的MSP

在建立一个新的Fabric CA后以及注册任意一个成员之前，首先需要获取Fabric CA初始成员的证书，否则无法使用该CA注册任何成员。在第1步中，`docker-compose-ca.yaml`文件中的command一项给出了初始成员的注册信息。

```yaml
command: sh -c 'fabric-ca-server start -b admin:adminpw -d'
```

可以得知初始成员的登录信息为`admin:adminpw`，然后通过以下命令即可获得初始成员的MSP。

```shell
export PATH=${PWD}/../../../bin:$PATH
export TLS_CERT_FILE=${PWD}/ca-home/tls-cert.pem
export FABRIC_CA_CLIENT_HOME=${PWD}/crypto-config

fabric-ca-client enroll -u https://admin:adminpw@10.10.11.31:7054 --caname ca.cn --tls.certfiles ${TLS_CERT_FILE}
```

该成员相当于CA的管理员，之后所有其他成员的注册都需要使用该身份。

#### (2) 创建组织单位定义文件

在本系统中，使用组织单位(OU)来辅助管理不同身份类型的成员，组织单位的定义在`organizations/cn/crypto-config/msp/config.yaml`文件中。当前该文件采用fabric的默认配置，所有的组织单位的CA证书均采用组织CA根节点的CA证书。该文件的定义如下：

```yaml
NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/10-10-11-31-7054-ca-cn.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/10-10-11-31-7054-ca-cn.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/10-10-11-31-7054-ca-cn.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/10-10-11-31-7054-ca-cn.pem
    OrganizationalUnitIdentifier: orderer
```

如果Fabric网络开启了组织单位的选项，那么所有成员的msp都必须包含该文件。

#### (3) 在Fabric CA注册peer0.cn成员

注册（register）将会在Fabric CA数据库中增加该成员的记录，记录中存储了成员身份相关的信息，这些信息用于生成该成员的MSP。

注册peer0.cn成员需要执行如下命令。

```shell
export PATH=${PWD}/../../../bin:$PATH
export TLS_CERT_FILE=${PWD}/ca-home/tls-cert.pem
export FABRIC_CA_CLIENT_HOME=${PWD}/crypto-config # 通过指定FABRIC_CA_CLIENT_HOME来指定注册者的MSP，此处为初始成员

fabric-ca-client register --caname ca.cn --id.name peer0.cn --id.secret peer0pw --id.type peer --tls.certfiles ${TLS_CERT_FILE} -u "https://10.10.11.31:7054"
```

执行该操作后，ca.cn将会增加一条名为peer0.cn的成员记录，该成员类型为peer，导出证书的密钥为peer0pw。

#### (4) 获取peer0.cn成员的MSP

导出证书（enroll）将会根据Fabric CA中相应成员的记录生成该成员的MSP，具体命令如下：

```shell
export PATH=${PWD}/../../../bin:$PATH
export TLS_CERT_FILE=${PWD}/ca-home/tls-cert.pem
export FABRIC_CA_CLIENT_HOME=${PWD}/crypto-config 

fabric-ca-client enroll -u https://peer0.cn:peer0pw@10.10.11.31:7054 --caname ca.cn -M ${FABRIC_CA_CLIENT_HOME}/peers/peer0.cn/msp --csr.hosts peer0.cn --tls.certfiles ${TLS_CERT_FILE}

cp ${FABRIC_CA_CLIENT_HOME}/msp/config.yaml ${FABRIC_CA_CLIENT_HOME}/peers/peer0.cn/msp/config.yaml
```

最终生成的成员MSP将会存放在`${FABRIC_CA_CLIENT_HOME}/peers/peer0.cn/msp`目录下。最后将组织单位定义文件复制到该成员的msp即可。

#### (5) 获取peer0.cn成员的TLS连接信息

该步骤只有需要进行TLS通信的成员进行。具体的命令如下：

```shell
export PATH=${PWD}/../../../bin:$PATH
export TLS_CERT_FILE=${PWD}/ca-home/tls-cert.pem
export FABRIC_CA_CLIENT_HOME=${PWD}/crypto-config 

fabric-ca-client enroll -u https://peer0.cn:peer0pw@10.10.11.31:7054 --caname ca.cn -M ${FABRIC_CA_CLIENT_HOME}/peers/peer0.cn/tls --enrollment.profile tls --csr.hosts peer0.cn --csr.hosts localhost --tls.certfiles ${TLS_CERT_FILE}

# 生成peer0.cn的TLS连接信息
cp ${FABRIC_CA_CLIENT_HOME}/peers/peer0.cn/tls/tlscacerts/* ${FABRIC_CA_CLIENT_HOME}/peers/peer0.cn/tls/ca.crt
cp ${FABRIC_CA_CLIENT_HOME}/peers/peer0.cn/tls/signcerts/* ${FABRIC_CA_CLIENT_HOME}/peers/peer0.cn/tls/server.crt
cp ${FABRIC_CA_CLIENT_HOME}/peers/peer0.cn/tls/keystore/* ${FABRIC_CA_CLIENT_HOME}/peers/peer0.cn/tls/server.key

# 生成cn组织的TLS连接信息
mkdir -p ${FABRIC_CA_CLIENT_HOME}/msp/tlscacerts
cp ${FABRIC_CA_CLIENT_HOME}/peers/peer0.cn/tls/tlscacerts/* ${FABRIC_CA_CLIENT_HOME}/msp/tlscacerts/ca.crt
mkdir -p ${FABRIC_CA_CLIENT_HOME}/tlsca
cp ${FABRIC_CA_CLIENT_HOME}/peers/peer0.cn/tls/tlscacerts/* ${FABRIC_CA_CLIENT_HOME}/tlsca/tlsca.cn-cert.pem
mkdir -p ${FABRIC_CA_CLIENT_HOME}/ca
cp ${FABRIC_CA_CLIENT_HOME}/peers/peer0.cn/msp/cacerts/* ${FABRIC_CA_CLIENT_HOME}/ca/ca.cn-cert.pem

```

以上为生成peer0.cn的MSP和TLS连接信息的步骤，

修改上述命令行中的ip地址和端口，重复上述步骤可以生成其他成员的身份信息，详细的实现在各个组织下的`register-enroll.sh`文件中。


### 3. 编辑网络配置文件，生成建立区块链网络必需的文件

Fabric的网络配置文件`configtx.yaml`是Fabric区块链网络的核心配置文件，它定义了一个网络中的组织，通道等网络结构相关的重要信息，同时Fabric的`configtxgen`工具将会通过读取该文件生成区块链启动时所需的创世区块，通道配置文件和锚节点文件。

在本系统中，`configtx.yaml`主要的自定义项为组织定义项，orderer定义项和档案（profile）项。`configtx.yaml`的具体内容在附录中给出。



下面的命令已转移至后续步骤执行，这里仅作用途介绍。可跳转至第四步骤，继续手动部署。

在配置完毕后，使用以下命令用于生成Fabric区块链网络的创世区块。

```shell
export PATH=${PWD}/../../bin:$PATH
export FABRIC_CFG_PATH=${PWD}

configtxgen -profile LocalRootChainOrdererGenesis -channelID system-channel -outputBlock ./system-genesis-block/genesis.block
```

以下命令用于生成local-root-chain通道的配置区块

```shell
export PATH=${PWD}/../../bin:$PATH
export FABRIC_CFG_PATH=${PWD}
export CHANNEL_NAME=local-root-chain

configtxgen -profile LocalRootChainChannel -outputCreateChannelTx ./channel-artifacts/${CHANNEL_NAME}.tx -channelID $CHANNEL_NAME
```

以下命令用于生成Peer组织的锚节点文件

```shell
export PATH=${PWD}/../../bin:$PATH
export FABRIC_CFG_PATH=${PWD}
export CHANNEL_NAME=local-root-chain

configtxgen -profile LocalRootChainChannel -outputAnchorPeersUpdate ./channel-artifacts/cnMSPanchors.tx -channelID $CHANNEL_NAME -asOrg cnMSP
configtxgen -profile LocalRootChainChannel -outputAnchorPeersUpdate ./channel-artifacts/ruMSPanchors.tx -channelID $CHANNEL_NAME -asOrg ruMSP
configtxgen -profile LocalRootChainChannel -outputAnchorPeersUpdate ./channel-artifacts/ianaMSPanchors.tx -channelID $CHANNEL_NAME -asOrg ianaMSP
```

### 4. 创建Fabric网络中定义的节点

1. 在orderer组织所在主机上，通过执行script目录下的`set-network.sh`，生成创世区块。
2. 在所有组织机器上，执行script目录下的`org-node-start.sh`，启动各自组织的节点。






过程分析：

该步骤将会创建除了Fabric CA之外的所有节点。以cn组织为例，根据文档中**部署目标**中的需求，需要启动1个peer节点和1个外挂的couchDB。本系统中所有的节点和外挂服务均以docker容器的形式出现，下列部署文件一方面给出了节点的部署选项，另一方面通过传入环境变量来修改peer节点本身的配置。

```YAML
version: '2'

networks:
  localrootchain:

volumes:
  peer0.cn:

services:
  couchdb.peer0.cn:
    container_name: couchdb.peer0.cn
    image: couchdb:3.1
    environment:
      - COUCHDB_USER=admin
      - COUCHDB_PASSWORD=adminpw
    ports:
      - "$COUCHDB_PEER0_PORT:5984"
    networks:
      - localrootchain

  peer0.cn:
    container_name: peer0.cn
    image: hyperledger/fabric-peer:$IMAGE_TAG
    environment:
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=${COMPOSE_PROJECT_NAME}_localrootchain
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_PROFILE_ENABLED=true
      - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
      - CORE_PEER_ID=peer0.cn
      - CORE_PEER_ADDRESS=peer0.cn:$PEER0_PORT
      - CORE_PEER_LISTENADDRESS=0.0.0.0:$PEER0_PORT
      - CORE_PEER_CHAINCODEADDRESS=peer0.cn:7052
      - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:7052
      - CORE_PEER_GOSSIP_BOOTSTRAP=peer0.cn:$PEER0_PORT
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer0.cn:$PEER0_PORT
      - CORE_PEER_LOCALMSPID=cnMSP
      - CORE_LEDGER_STATE_STATEDATABASE=CouchDB
      - CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=couchdb.peer0.cn:$COUCHDB_PEER0_PORT
      - CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME=admin
      - CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD=adminpw
    volumes:
        - /var/run/:/host/var/run/
        - ./crypto-config/peers/peer0.cn/msp:/etc/hyperledger/fabric/msp
        - ./crypto-config/peers/peer0.cn/tls:/etc/hyperledger/fabric/tls
        - peer0.cn:/var/hyperledger/production
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    command: peer node start
    ports:
      - $PEER0_PORT:7050
    depends_on:
      - couchdb.peer0.cn
    networks:
      - localrootchain
```

通过以下命令启动cn组织的所有节点。

```
docker-compose up -d
```

其他组织节点部署的配置和启动方式参照cn组织的设置修改即可。具体的设置可以参照`organization/${orgName}`下的配置文件。

### 5. 创建通道，将节点加入通道

1. orderer节点所在物理机上：于script目录下执行`./create_channel.sh local-root-chain`命令，创建名为local-root-chain的通道。
2. 其他所有物理机上：运行script目录下的`./get-channelblock-chaincodepackage.sh -c`脚本，获取通道block
3. 所有主机：于script目录下执行`./join-channel-org.sh local-root-chain`命令，将节点加入通道并设置锚节点






过程分析:

- 比如要创建名为local-root-chain的通道，具体使用方法如下，在orderer节点执行

```shell
export CHANNEL_NAME=local-root-chain
./create_channel.sh $CHANNEL_NAME
```

`create_channel.sh`脚本内容如下

```shell
export PATH=${PWD}/../../bin:$PATH
export FABRIC_CFG_PATH=${PWD}/config
export CHANNEL_NAME=local-root-chain

export CORE_PEER_LOCALMSPID="cnMSP"
export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_CN_CA
export CORE_PEER_MSPCONFIGPATH=${PWD}/cn/crypto-config/users/admin@cn/msp
export CORE_PEER_ADDRESS=10.10.11.31:7050
export ORDERER0_CA=${PWD}/orderer/crypto-config/orderers/orderer0.orderer/msp/tlscacerts/tlsca.orderer-cert.pem
```

指定了必要的环境变量后，在区块链网络内建立通道，命令如下：

```shell
peer channel create -o 10.10.11.11:11050 -c $CHANNEL_NAME --ordererTLSHostnameOverride orderer0.orderer -f ./channel-artifacts/${CHANNEL_NAME}.tx --outputBlock ./channel-artifacts/${CHANNEL_NAME}.block --tls --cafile $ORDERER0_CA
```

- 然后将上面生成的block通过scp发送到各个机器.

在所有节点建立之后，需要将所有组织加入通道中，**只有加入到同一个通道中的组织才能相互连接并交互**。

- 然后节点加入通道。以cn为例：

Fabric自带的peer工具可以指定当前操作的peer节点并发送相应的命令，以cn组织为例，首先需要指定当前使用的节点为peer0.cn，命令如下：

将peer0.cn节点加入通道中，命令为

随后将peer0.cn节点加入通道中，命令为

```shell
export PATH=${PWD}/../../bin:$PATH
export FABRIC_CFG_PATH=${PWD}/config
export CHANNEL_NAME=local-root-chain

export CORE_PEER_LOCALMSPID="cnMSP"
export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_CN_CA
export CORE_PEER_MSPCONFIGPATH=${PWD}/cn/crypto-config/users/admin@cn/msp
export CORE_PEER_ADDRESS=10.10.11.31:7050

peer channel join -b ./channel-artifacts/$CHANNEL_NAME.block
```

最后将peer0.cn设置为锚节点，让不同组织之间的节点可以与cn组织通信，以peer0.china节点为例，命令为

```shell
peer channel update -o 10.10.11.11:11050 --ordererTLSHostnameOverride orderer0.orderer -c $CHANNEL_NAME -f ./channel-artifacts/${CORE_PEER_LOCALMSPID}anchors.tx --tls --cafile $ORDERER0_CA
```

其他组织的节点加入通道的步骤与上面的命令类似，在各个主机执行即可，注意环境变量的不同 (ip,端口,msp等)。



### 6. 部署链码

脚本的执行分三步：

1. hit组织所在物理机：于script目录运行`pack-chaincode.sh`脚本 和`install-approve-chaincode.sh`脚本
2. 在其他所有主机：运行`./get-channelblock-chaincodepackage.sh -p`脚本和`install-approve-chaincode.sh`脚本
3. hit组织所在物理机：运行`commit-chaincode.sh`脚本





过程分析：

- <span style='color:red;background:white;font-size:20px;font-family:微软雅黑;'>cn节点，运行script目录下的`handle-chaincode.sh`脚本。</span>

  该脚本包括：打包链码，在cn节点安装3链码，批准3链码，并将之前打包链码生成的文件发送到其他主机。

  具体调用方式如下：

```shell
./pack-install-approve-cn.sh local-root-chain LocalRootMembershipCC ../../chaincode/membershipCC 1.0 1 NA
sleep 5
./pack-install-approve-cn.sh local-root-chain LocalRootServiceCC ../../chaincode/localrootserviceCC 1.0 1 NA
sleep 5
./pack-install-approve-cn.sh local-root-chain TLDDelegationCC ../../chaincode/delegationCC 1.0 1 NA


./push-chaincode-package.sh
```

在Fabric v2.0之后，部署链码的流程如下：

```mermaid
graph LR
A(打包链码) --> B(安装链码)
B --> C(批准链码)
C --> D(提交链码)
```

现以部署链码LocalRootMembershipCC为例来说明上述的部署步骤，在这之前，先指定一些环境变量。

```shell
export PATH=${PWD}/../../bin:$PATH
export CHANNEL_NAME=local-root-chain
export CC_NAME=LocalRootMembershipCC
export CC_SRC_PATH=../../chaincode/membershipCC
export CC_VERSION=1.0
export CC_SEQUENCE=1

export ORDERER0_CA=${PWD}/orderer/crypto-config/orderers/orderer0.orderer/msp/tlscacerts/tlsca.orderer-cert.pem
export PEER0_CN_CA=${PWD}/cn/crypto-config/peers/peer0.cn/tls/ca.crt
export PEER0_RU_CA=${PWD}/ru/crypto-config/peers/peer0.ru/tls/ca.crt
export PEER0_IANA_CA=${PWD}/iana/crypto-config/peers/peer0.iana/tls/ca.crt
```

#### (1) 打包链码

该步骤会将一个golang链码项目打包并增加一些元信息，最终将会生成一个压缩包，该命令只需执行一次，具体命令如下：

```shell
peer lifecycle chaincode package ${CC_NAME}.tar.gz --path ${CC_SRC_PATH} --lang golang --label ${CC_NAME}_${CC_VERSION}
```

生成的压缩包将会出现在命令执行时的目录上。

#### (2) 安装链码

该步骤会将链码压缩包安装到一个peer节点上，以安装到peer0.cn节点上为例，安装链码的命令如下

```shell
export CORE_PEER_LOCALMSPID="cnMSP"
export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_CN_CA
export CORE_PEER_MSPCONFIGPATH=${PWD}/cn/crypto-config/users/admin@cn/msp
export CORE_PEER_ADDRESS=10.10.11.31:7050

peer lifecycle chaincode install ${CC_NAME}.tar.gz
```

在其他节点上安装链码只需将环境变量更改为相应的值即可。

链码安装完成后，通过以下命令确定链码是否成功安装，并获取该链码的**package_id**，这个ID在之后的步骤会用到。

```shell
peer lifecycle chaincode queryinstalled >&log.txt
PACKAGE_ID=$(sed -n "/${CC_NAME}_${CC_VERSION}/{s/^Package ID: //; s/, Label:.*$//; p;}" log.txt)
```

#### (3) 批准链码

在一个peer节点安装链码后，仅仅说明当前节点持有该链码。在这之后，一方面需要peer组织来决定是否应用该链码，另一方面需要通知区块链网络，该组织已经批准部署该链码，以cn组织为例，批准链码的命令如下：

```shell
export CORE_PEER_LOCALMSPID="cnMSP"
export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_CN_CA
export CORE_PEER_MSPCONFIGPATH=${PWD}/cn/crypto-config/users/admin@cn/msp
export CORE_PEER_ADDRESS=10.10.11.31:7050

peer lifecycle chaincode approveformyorg -o 10.10.11.11:11050 --ordererTLSHostnameOverride orderer0.orderer --tls --cafile $ORDERER0_CA --channelID $CHANNEL_NAME --name ${CC_NAME} --version ${CC_VERSION} --package-id ${PACKAGE_ID} --sequence ${CC_SEQUENCE}
```

在cn组织在打包，安装，批准链码之后，将打包文件发送到其他主机。

- <span style='color:blue;background:white;font-size:20px;font-family:微软雅黑;'>在其他节点诸如ru/iana上，执行`handle-chaincode.sh`：安装链码和批准链码。</span>

  环境变量与cn有所不同，注意差异。

#### (4) 提交链码

- <span style='color:green;background:white;font-size:20px;font-family:微软雅黑;'>在cn节点执行`commit-3-chaincode.sh`脚本，提交链码</span>

  当有足够多的组织批准链码之后（具体数目需要依网络配置文件中设定的策略决定），最后由一个相关的peer节点提交该链码定义，链码只有在提交后才能在区块链中使用。该命令只需执行一次，具体命令如下：

```shell
export CORE_PEER_LOCALMSPID="cnMSP"
export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_CN_CA
export CORE_PEER_MSPCONFIGPATH=${PWD}/cn/crypto-config/users/admin@cn/msp
export CORE_PEER_ADDRESS=10.10.11.31:7050

peer lifecycle chaincode commit -o 10.10.11.11:11050 --ordererTLSHostnameOverride orderer0.orderer --tls --cafile $ORDERER0_CA --channelID $CHANNEL_NAME --name ${CC_NAME} --peerAddresses 10.10.11.31:7050 --tlsRootCertFiles ${PEER0_CN_CA} --peerAddresses 10.10.11.130:8050 --tlsRootCertFiles ${PEER0_RU_CA} --peerAddresses 10.10.11.131:9050 --tlsRootCertFiles ${PEER0_IANA_CA} --version ${CC_VERSION} --sequence ${CC_SEQUENCE}
```

### 7. 连接客户端

TODO: 


## 附录

### configtx.yaml文件的配置项

```YAML
--- 
Organizations: # 列出该网络中所有的组织
    - &orderer # 排序组织
        Name: ordererMSP # 排序组织的名称
        ID: ordererMSP # 排序组织的MSPID
        MSPDir: orderer/crypto-config/msp # 排序组织的MSP目录
        Policies:
            Readers:
                Type: Signature
                Rule: "OR('ordererMSP.member')"
            Writers:
                Type: Signature
                Rule: "OR('ordererMSP.member')"
            Admins:
                Type: Signature
                Rule: "OR('ordererMSP.admin')"
        OrdererEndpoints:
            - 10.10.11.11:11050
    - &cn
        Name: cnMSP
        ID: cnMSP
        MSPDir: cn/crypto-config/msp
        Policies:
            Readers:
                Type: Signature
                Rule: "OR('cnMSP.admin', 'cnMSP.peer', 'cnMSP.client')"
            Writers:
                Type: Signature
                Rule: "OR('cnMSP.admin', 'cnMSP.client')"
            Admins:
                Type: Signature
                Rule: "OR('cnMSP.admin')"
            Endorsement:
                Type: Signature
                Rule: "OR('cnMSP.peer')"
        AnchorPeers:
            - Host: 10.10.11.31
              Port: 7050
    - &ru
        Name: ruMSP
        ID: ruMSP
        MSPDir: ru/crypto-config/msp
        Policies:
            Readers:
                Type: Signature
                Rule: "OR('ruMSP.admin', 'ruMSP.peer', 'ruMSP.client')"
            Writers:
                Type: Signature
                Rule: "OR('ruMSP.admin', 'ruMSP.client')"
            Admins:
                Type: Signature
                Rule: "OR('ruMSP.admin')"
            Endorsement:
                Type: Signature
                Rule: "OR('ruMSP.peer')"
        AnchorPeers:
            - Host: 10.10.11.130
              Port: 8050
    - &iana
        Name: ianaMSP
        ID: ianaMSP
        MSPDir: iana/crypto-config/msp
        Policies:
            Readers:
                Type: Signature
                Rule: "OR('ianaMSP.admin', 'ianaMSP.peer', 'ianaMSP.client')"
            Writers:
                Type: Signature
                Rule: "OR('ianaMSP.admin', 'ianaMSP.client')"
            Admins:
                Type: Signature
                Rule: "OR('ianaMSP.admin')"
            Endorsement:
                Type: Signature
                Rule: "OR('ianaMSP.peer')"
        AnchorPeers:
            - Host: 10.10.11.131
              Port: 9050

Capabilities:
    Channel: &ChannelCapabilities
        V2_0: true
    Orderer: &OrdererCapabilities
        V2_0: true
    Application: &ApplicationCapabilities
        V2_0: true

Application: &ApplicationDefaults
    Organizations:
    Policies:
        Readers:
            Type: ImplicitMeta
            Rule: "ANY Readers"
        Writers:
            Type: ImplicitMeta
            Rule: "ANY Writers"
        Admins:
            Type: ImplicitMeta
            Rule: "ANY Admins"
        LifecycleEndorsement:
            Type: ImplicitMeta
            Rule: "ANY Endorsement"
        Endorsement:
            Type: ImplicitMeta
            Rule: "ANY Endorsement"
    Capabilities:
        <<: *ApplicationCapabilities

Orderer: &OrdererDefaults
    OrdererType: etcdraft
    EtcdRaft:
        Consenters:
        - Host: orderer0.orderer
          Port: 11050
          ClientTLSCert: orderer/crypto-config/orderers/orderer0.orderer/tls/server.crt
          ServerTLSCert: orderer/crypto-config/orderers/orderer0.orderer/tls/server.crt
	BatchTimeout: 2s	# 区块分割等待时间
    BatchSize:			# 区块容量信息
        MaxMessageCount: 10			# 区块存储最大交易量
        AbsoluteMaxBytes: 99 MB		# 区块最大大小
        PreferredMaxBytes: 512 KB	# 区块大于此大小可能会分割
    Organizations:
    Policies:
        Readers:
            Type: ImplicitMeta
            Rule: "ANY Readers"
        Writers:
            Type: ImplicitMeta
            Rule: "ANY Writers"
        Admins:
            Type: ImplicitMeta
            Rule: "ANY Admins"
        BlockValidation:
            Type: ImplicitMeta
            Rule: "ANY Writers"

Channel: &ChannelDefaults
    Policies:
        Readers:
            Type: ImplicitMeta
            Rule: "ANY Readers"
        Writers:
            Type: ImplicitMeta
            Rule: "ANY Writers"
        Admins:
            Type: ImplicitMeta
            Rule: "ANY Admins"
    Capabilities:
        <<: *ChannelCapabilities


Profiles:
    LocalRootChainOrdererGenesis:
        <<: *ChannelDefaults
        Orderer:
            <<: *OrdererDefaults
            Organizations:
                - *orderer
            Capabilities:
                <<: *OrdererCapabilities
        Consortiums:
            LocalRootChainConsortium:
                Organizations:
                    - *cn
                    - *ru
                    - *iana
    LocalRootChainChannel:
        Consortium: LocalRootChainConsortium
        <<: *ChannelDefaults
        Application:
            <<: *ApplicationDefaults
            Organizations:
                - *cn
                - *ru
                - *iana
            Capabilities:
                <<: *ApplicationCapabilities
```

