#!/bin/bash
#

export PORT=10054
export ORG_NAME=ncse

export PATH=${PWD}/../../../bin:$PATH
export FABRIC_CA_CLIENT_HOME=${PWD}/crypto-config
export CA_NAME=ca.${ORG_NAME}
export TLS_CERT_FILE=${PWD}/ca-home/tls-cert.pem

. ../../scripts/env-var.sh
destIP=$org5IP
IP1st=`echo $destIP | cut -d '.' -f 1`
IP2nd=`echo $destIP | cut -d '.' -f 2`
IP3rd=`echo $destIP | cut -d '.' -f 3`
IP4th=`echo $destIP | cut -d '.' -f 4`

echo
echo "Enroll the CA admin"
echo
set -x
fabric-ca-client enroll -u https://admin:adminpw@${destIP}:${PORT} --caname ${CA_NAME} --tls.certfiles ${TLS_CERT_FILE}
set +x

echo "NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/${IP1st}-${IP2nd}-${IP3rd}-${IP4th}-${PORT}-ca-${ORG_NAME}.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/${IP1st}-${IP2nd}-${IP3rd}-${IP4th}-${PORT}-ca-${ORG_NAME}.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/${IP1st}-${IP2nd}-${IP3rd}-${IP4th}-${PORT}-ca-${ORG_NAME}.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/${IP1st}-${IP2nd}-${IP3rd}-${IP4th}-${PORT}-ca-${ORG_NAME}.pem
    OrganizationalUnitIdentifier: orderer" > ${FABRIC_CA_CLIENT_HOME}/msp/config.yaml

echo
echo "Register peer0 ~ peer4"
echo
set -x
fabric-ca-client register --caname ${CA_NAME} --id.name peer0.${ORG_NAME} --id.secret peer0pw --id.type peer --tls.certfiles ${TLS_CERT_FILE} -u "https://${destIP}:${PORT}"
fabric-ca-client register --caname ${CA_NAME} --id.name peer1.${ORG_NAME} --id.secret peer1pw --id.type peer --tls.certfiles ${TLS_CERT_FILE} -u "https://${destIP}:${PORT}"
fabric-ca-client register --caname ${CA_NAME} --id.name peer2.${ORG_NAME} --id.secret peer2pw --id.type peer --tls.certfiles ${TLS_CERT_FILE} -u "https://${destIP}:${PORT}"
fabric-ca-client register --caname ${CA_NAME} --id.name peer3.${ORG_NAME} --id.secret peer3pw --id.type peer --tls.certfiles ${TLS_CERT_FILE} -u "https://${destIP}:${PORT}"
fabric-ca-client register --caname ${CA_NAME} --id.name peer4.${ORG_NAME} --id.secret peer4pw --id.type peer --tls.certfiles ${TLS_CERT_FILE} -u "https://${destIP}:${PORT}"
set +x

echo
echo "Register users"
echo
set -x
fabric-ca-client register --caname ${CA_NAME} --id.name authority1.${ORG_NAME}  --id.secret authority1pw --id.type client --tls.certfiles ${TLS_CERT_FILE} -u "https://${destIP}:${PORT}"
fabric-ca-client register --caname ${CA_NAME} --id.name authority2.${ORG_NAME}  --id.secret authority2pw --id.type client --tls.certfiles ${TLS_CERT_FILE} -u "https://${destIP}:${PORT}"
fabric-ca-client register --caname ${CA_NAME} --id.name authority3.${ORG_NAME}  --id.secret authority3pw --id.type client --tls.certfiles ${TLS_CERT_FILE} -u "https://${destIP}:${PORT}"
fabric-ca-client register --caname ${CA_NAME} --id.name operator1.${ORG_NAME}  --id.secret operator1pw --id.type client --tls.certfiles ${TLS_CERT_FILE} -u "https://${destIP}:${PORT}"
fabric-ca-client register --caname ${CA_NAME} --id.name operator2.${ORG_NAME}  --id.secret operator2pw --id.type client --tls.certfiles ${TLS_CERT_FILE} -u "https://${destIP}:${PORT}"
fabric-ca-client register --caname ${CA_NAME} --id.name operator3.${ORG_NAME}  --id.secret operator3pw --id.type client --tls.certfiles ${TLS_CERT_FILE} -u "https://${destIP}:${PORT}"
fabric-ca-client register --caname ${CA_NAME} --id.name maintainer1.${ORG_NAME}  --id.secret maintainer1pw --id.type client --tls.certfiles ${TLS_CERT_FILE} -u "https://${destIP}:${PORT}"
fabric-ca-client register --caname ${CA_NAME} --id.name maintainer2.${ORG_NAME}  --id.secret maintainer2pw --id.type client --tls.certfiles ${TLS_CERT_FILE} -u "https://${destIP}:${PORT}"
fabric-ca-client register --caname ${CA_NAME} --id.name maintainer3.${ORG_NAME}  --id.secret maintainer3pw --id.type client --tls.certfiles ${TLS_CERT_FILE} -u "https://${destIP}:${PORT}"

set +x

echo
echo "Register the admin"
echo
set -x
fabric-ca-client register --caname ${CA_NAME} --id.name admin.${ORG_NAME}  --id.secret adminpw --id.type admin --tls.certfiles ${TLS_CERT_FILE} -u "https://${destIP}:${PORT}"
set +x

mkdir -p ${FABRIC_CA_CLIENT_HOME}/peers
mkdir -p ${FABRIC_CA_CLIENT_HOME}/peers/peer0.${ORG_NAME}

echo
echo "## Generate the peer0 msp"
echo
set -x
fabric-ca-client enroll -u https://peer0.${ORG_NAME}:peer0pw@${destIP}:${PORT} --caname ${CA_NAME} -M ${FABRIC_CA_CLIENT_HOME}/peers/peer0.${ORG_NAME}/msp --csr.hosts peer0.${ORG_NAME} --tls.certfiles ${TLS_CERT_FILE}
set +x

cp ${FABRIC_CA_CLIENT_HOME}/msp/config.yaml ${FABRIC_CA_CLIENT_HOME}/peers/peer0.${ORG_NAME}/msp/config.yaml

echo
echo "## Generate the peer0-tls certificates"
echo
set -x
fabric-ca-client enroll -u https://peer0.${ORG_NAME}:peer0pw@${destIP}:${PORT} --caname ${CA_NAME} -M ${FABRIC_CA_CLIENT_HOME}/peers/peer0.${ORG_NAME}/tls --enrollment.profile tls --csr.hosts peer0.${ORG_NAME} --csr.hosts ${destIP} --tls.certfiles ${TLS_CERT_FILE}
set +x


cp ${FABRIC_CA_CLIENT_HOME}/peers/peer0.${ORG_NAME}/tls/tlscacerts/* ${FABRIC_CA_CLIENT_HOME}/peers/peer0.${ORG_NAME}/tls/ca.crt
cp ${FABRIC_CA_CLIENT_HOME}/peers/peer0.${ORG_NAME}/tls/signcerts/* ${FABRIC_CA_CLIENT_HOME}/peers/peer0.${ORG_NAME}/tls/server.crt
cp ${FABRIC_CA_CLIENT_HOME}/peers/peer0.${ORG_NAME}/tls/keystore/* ${FABRIC_CA_CLIENT_HOME}/peers/peer0.${ORG_NAME}/tls/server.key

mkdir -p ${FABRIC_CA_CLIENT_HOME}/msp/tlscacerts
cp ${FABRIC_CA_CLIENT_HOME}/peers/peer0.${ORG_NAME}/tls/tlscacerts/* ${FABRIC_CA_CLIENT_HOME}/msp/tlscacerts/ca.crt

mkdir -p ${FABRIC_CA_CLIENT_HOME}/tlsca
cp ${FABRIC_CA_CLIENT_HOME}/peers/peer0.${ORG_NAME}/tls/tlscacerts/* ${FABRIC_CA_CLIENT_HOME}/tlsca/tlsca.${ORG_NAME}-cert.pem

mkdir -p ${FABRIC_CA_CLIENT_HOME}/ca
cp ${FABRIC_CA_CLIENT_HOME}/peers/peer0.${ORG_NAME}/msp/cacerts/* ${FABRIC_CA_CLIENT_HOME}/ca/ca.${ORG_NAME}-cert.pem

mkdir -p ${FABRIC_CA_CLIENT_HOME}/users
mkdir -p ${FABRIC_CA_CLIENT_HOME}/users/authority1@${ORG_NAME}
mkdir -p ${FABRIC_CA_CLIENT_HOME}/users/authority2@${ORG_NAME}
mkdir -p ${FABRIC_CA_CLIENT_HOME}/users/authority3@${ORG_NAME}
mkdir -p ${FABRIC_CA_CLIENT_HOME}/users/operator1@${ORG_NAME}
mkdir -p ${FABRIC_CA_CLIENT_HOME}/users/operator2@${ORG_NAME}
mkdir -p ${FABRIC_CA_CLIENT_HOME}/users/operator3@${ORG_NAME}
mkdir -p ${FABRIC_CA_CLIENT_HOME}/users/maintainer1@${ORG_NAME}
mkdir -p ${FABRIC_CA_CLIENT_HOME}/users/maintainer2@${ORG_NAME}
mkdir -p ${FABRIC_CA_CLIENT_HOME}/users/maintainer3@${ORG_NAME}

echo
echo "## Generate the users msp"
echo
set -x
fabric-ca-client enroll -u https://authority1.${ORG_NAME}:authority1pw@${destIP}:${PORT} --caname ${CA_NAME} -M ${FABRIC_CA_CLIENT_HOME}/users/authority1@${ORG_NAME}/msp --tls.certfiles ${TLS_CERT_FILE}
fabric-ca-client enroll -u https://authority2.${ORG_NAME}:authority2pw@${destIP}:${PORT} --caname ${CA_NAME} -M ${FABRIC_CA_CLIENT_HOME}/users/authority2@${ORG_NAME}/msp --tls.certfiles ${TLS_CERT_FILE}
fabric-ca-client enroll -u https://authority3.${ORG_NAME}:authority3pw@${destIP}:${PORT} --caname ${CA_NAME} -M ${FABRIC_CA_CLIENT_HOME}/users/authority3@${ORG_NAME}/msp --tls.certfiles ${TLS_CERT_FILE}
fabric-ca-client enroll -u https://operator1.${ORG_NAME}:operator1pw@${destIP}:${PORT} --caname ${CA_NAME} -M ${FABRIC_CA_CLIENT_HOME}/users/operator1@${ORG_NAME}/msp --tls.certfiles ${TLS_CERT_FILE}
fabric-ca-client enroll -u https://operator2.${ORG_NAME}:operator2pw@${destIP}:${PORT} --caname ${CA_NAME} -M ${FABRIC_CA_CLIENT_HOME}/users/operator2@${ORG_NAME}/msp --tls.certfiles ${TLS_CERT_FILE}
fabric-ca-client enroll -u https://operator3.${ORG_NAME}:operator3pw@${destIP}:${PORT} --caname ${CA_NAME} -M ${FABRIC_CA_CLIENT_HOME}/users/operator3@${ORG_NAME}/msp --tls.certfiles ${TLS_CERT_FILE}
fabric-ca-client enroll -u https://maintainer1.${ORG_NAME}:maintainer1pw@${destIP}:${PORT} --caname ${CA_NAME} -M ${FABRIC_CA_CLIENT_HOME}/users/maintainer1@${ORG_NAME}/msp --tls.certfiles ${TLS_CERT_FILE}
fabric-ca-client enroll -u https://maintainer2.${ORG_NAME}:maintainer2pw@${destIP}:${PORT} --caname ${CA_NAME} -M ${FABRIC_CA_CLIENT_HOME}/users/maintainer2@${ORG_NAME}/msp --tls.certfiles ${TLS_CERT_FILE}
fabric-ca-client enroll -u https://maintainer3.${ORG_NAME}:maintainer3pw@${destIP}:${PORT} --caname ${CA_NAME} -M ${FABRIC_CA_CLIENT_HOME}/users/maintainer3@${ORG_NAME}/msp --tls.certfiles ${TLS_CERT_FILE}
set +x

cp ${FABRIC_CA_CLIENT_HOME}/msp/config.yaml ${FABRIC_CA_CLIENT_HOME}/users/authority1@${ORG_NAME}/msp/config.yaml
cp ${FABRIC_CA_CLIENT_HOME}/msp/config.yaml ${FABRIC_CA_CLIENT_HOME}/users/authority2@${ORG_NAME}/msp/config.yaml
cp ${FABRIC_CA_CLIENT_HOME}/msp/config.yaml ${FABRIC_CA_CLIENT_HOME}/users/authority3@${ORG_NAME}/msp/config.yaml
cp ${FABRIC_CA_CLIENT_HOME}/msp/config.yaml ${FABRIC_CA_CLIENT_HOME}/users/operator1@${ORG_NAME}/msp/config.yaml
cp ${FABRIC_CA_CLIENT_HOME}/msp/config.yaml ${FABRIC_CA_CLIENT_HOME}/users/operator2@${ORG_NAME}/msp/config.yaml
cp ${FABRIC_CA_CLIENT_HOME}/msp/config.yaml ${FABRIC_CA_CLIENT_HOME}/users/operator3@${ORG_NAME}/msp/config.yaml
cp ${FABRIC_CA_CLIENT_HOME}/msp/config.yaml ${FABRIC_CA_CLIENT_HOME}/users/maintainer1@${ORG_NAME}/msp/config.yaml
cp ${FABRIC_CA_CLIENT_HOME}/msp/config.yaml ${FABRIC_CA_CLIENT_HOME}/users/maintainer2@${ORG_NAME}/msp/config.yaml
cp ${FABRIC_CA_CLIENT_HOME}/msp/config.yaml ${FABRIC_CA_CLIENT_HOME}/users/maintainer3@${ORG_NAME}/msp/config.yaml

mkdir -p ${FABRIC_CA_CLIENT_HOME}/users/admin@${ORG_NAME}

echo
echo "## Generate the org admin msp"
echo
set -x
fabric-ca-client enroll -u https://admin.${ORG_NAME}:adminpw@${destIP}:${PORT} --caname ${CA_NAME} -M ${FABRIC_CA_CLIENT_HOME}/users/admin@${ORG_NAME}/msp --tls.certfiles ${TLS_CERT_FILE}
set +x

cp ${FABRIC_CA_CLIENT_HOME}/msp/config.yaml ${FABRIC_CA_CLIENT_HOME}/users/admin@${ORG_NAME}/msp/config.yaml



