#!/bin/bash
#

export PORT=11054
export ORG_NAME=orderer

export PATH=${PWD}/../../../bin:$PATH
export FABRIC_CA_CLIENT_HOME=${PWD}/crypto-config
export CA_NAME=ca.${ORG_NAME}
export TLS_CERT_FILE=${PWD}/ca-home/tls-cert.pem

. ../../scripts/env-var.sh
destIP=$org1IP
IP1st=`echo $destIP | cut -d '.' -f 1`
IP2nd=`echo $destIP | cut -d '.' -f 2`
IP3rd=`echo $destIP | cut -d '.' -f 3`
IP4th=`echo $destIP | cut -d '.' -f 4`

set -x
fabric-ca-client enroll -u https://admin:adminpw@${destIP}:${PORT} --caname ${CA_NAME} --tls.certfiles ${TLS_CERT_FILE}
set +x

echo "NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/${IP1st}-${IP2nd}-${IP3rd}-${IP4th}-${PORT}-ca-${ORG_NAME}.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/${IP1st}-${IP2nd}-${IP3rd}-${IP4th}-${PORT}-ca-${ORG_NAME}.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/${IP1st}-${IP2nd}-${IP3rd}-${IP4th}-${PORT}-ca-${ORG_NAME}.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/${IP1st}-${IP2nd}-${IP3rd}-${IP4th}-${PORT}-ca-${ORG_NAME}.pem
    OrganizationalUnitIdentifier: orderer" > ${FABRIC_CA_CLIENT_HOME}/msp/config.yaml

echo
echo "Register orderer0"
echo
set -x
fabric-ca-client register --caname ${CA_NAME} --id.name orderer0.${ORG_NAME} --id.secret orderer0pw --id.type orderer --tls.certfiles ${TLS_CERT_FILE}
set +x

echo
echo "Register the admin"
echo
set -x
fabric-ca-client register --caname ${CA_NAME} --id.name admin.${ORG_NAME}  --id.secret adminpw --id.type admin --tls.certfiles ${TLS_CERT_FILE}
set +x

mkdir -p ${FABRIC_CA_CLIENT_HOME}/orderers
mkdir -p ${FABRIC_CA_CLIENT_HOME}/orderers/orderer0.${ORG_NAME}

echo
echo "## Generate the orderer0 msp"
echo
set -x
fabric-ca-client enroll -u https://orderer0.${ORG_NAME}:orderer0pw@${destIP}:${PORT} --caname ${CA_NAME} -M ${FABRIC_CA_CLIENT_HOME}/orderers/orderer0.${ORG_NAME}/msp --csr.hosts orderer0.${ORG_NAME} --tls.certfiles ${TLS_CERT_FILE}
set +x

cp ${FABRIC_CA_CLIENT_HOME}/msp/config.yaml ${FABRIC_CA_CLIENT_HOME}/orderers/orderer0.${ORG_NAME}/msp/config.yaml

echo
echo "## Generate the orderer0-tls certificates"
echo
set -x
fabric-ca-client enroll -u https://orderer0.${ORG_NAME}:orderer0pw@${destIP}:${PORT} --caname ${CA_NAME} -M ${FABRIC_CA_CLIENT_HOME}/orderers/orderer0.${ORG_NAME}/tls --enrollment.profile tls --csr.hosts orderer0.${ORG_NAME} --csr.hosts ${destIP} --tls.certfiles ${TLS_CERT_FILE}
set +x


cp ${FABRIC_CA_CLIENT_HOME}/orderers/orderer0.${ORG_NAME}/tls/tlscacerts/* ${FABRIC_CA_CLIENT_HOME}/orderers/orderer0.${ORG_NAME}/tls/ca.crt
cp ${FABRIC_CA_CLIENT_HOME}/orderers/orderer0.${ORG_NAME}/tls/signcerts/* ${FABRIC_CA_CLIENT_HOME}/orderers/orderer0.${ORG_NAME}/tls/server.crt
cp ${FABRIC_CA_CLIENT_HOME}/orderers/orderer0.${ORG_NAME}/tls/keystore/* ${FABRIC_CA_CLIENT_HOME}/orderers/orderer0.${ORG_NAME}/tls/server.key

mkdir -p ${FABRIC_CA_CLIENT_HOME}/orderers/orderer0.${ORG_NAME}/msp/tlscacerts
cp ${FABRIC_CA_CLIENT_HOME}/orderers/orderer0.${ORG_NAME}/tls/tlscacerts/* ${FABRIC_CA_CLIENT_HOME}/orderers/orderer0.${ORG_NAME}/msp/tlscacerts/tlsca.${ORG_NAME}-cert.pem

mkdir -p ${FABRIC_CA_CLIENT_HOME}/msp/tlscacerts
cp ${FABRIC_CA_CLIENT_HOME}/orderers/orderer0.${ORG_NAME}/tls/tlscacerts/* ${FABRIC_CA_CLIENT_HOME}/msp/tlscacerts/tlsca.${ORG_NAME}-cert.pem

mkdir -p ${FABRIC_CA_CLIENT_HOME}/users
mkdir -p ${FABRIC_CA_CLIENT_HOME}/users/admin@${ORG_NAME}

echo
echo "## Generate the org admin msp"
echo
set -x
fabric-ca-client enroll -u https://admin.${ORG_NAME}:adminpw@${destIP}:${PORT} --caname ${CA_NAME} -M ${FABRIC_CA_CLIENT_HOME}/users/admin@${ORG_NAME}/msp --tls.certfiles ${TLS_CERT_FILE}
set +x

cp ${FABRIC_CA_CLIENT_HOME}/msp/config.yaml ${FABRIC_CA_CLIENT_HOME}/users/admin@${ORG_NAME}/msp/config.yaml



