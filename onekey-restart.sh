#!/bin/bash
#

# ./delete-network.sh
./network.sh down
sleep 2
# ./set-ca.sh

./network.sh up
sleep 3
./network.sh createC -c local-root-chain
sleep 5
./network.sh deployCC -c local-root-chain -ccn LocalRootMembershipCC -ccp ../../chaincode/membershipCC
sleep 5
./network.sh deployCC -c local-root-chain -ccn LocalRootServiceCC -ccp ../../chaincode/localrootserviceCC
sleep 5
./network.sh deployCC -c local-root-chain -ccn TLDDelegationCC -ccp ../../chaincode/delegationCC

