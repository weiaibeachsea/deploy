#!/bin/bash
#

cd ./scripts

# import utils
. env-var.sh

# Print the usage message
function printHelp() {
        echo "Usage: "
        echo "  network.sh <Mode> [Flags]"
        echo "    Modes:"
        echo "      "$'\e[0;32m'up$'\e[0m' - bring up fabric orderer and peer nodes. No channel is created
        echo "      "$'\e[0;32m'createC$'\e[0m' - create and join a channel after the network is created
        echo "      "$'\e[0;32m'deployCC$'\e[0m' - deploy the asset transfer basic chaincode on the channel or specify
        echo "      "$'\e[0;32m'down$'\e[0m' - clear the network with docker-compose down
        echo "      "$'\e[0;32m'restart$'\e[0m' - restart the network
    echo "      "$'\e[0;32m'setOrg$'\e[0m' - set environment variable to specific organizaion, please execute by source
        echo
        echo "    Flags:"
        echo "    Used with "$'\e[0;32m'network.sh createC$'\e[0m':
        echo "    -c <channel name> - channel name to use (defaults to \"mychannel\")"
        echo "    Used with "$'\e[0;32m'network.sh deployCC$'\e[0m'
        echo "    -c <channel name> - deploy chaincode to channel"
        echo "    -ccn <name> - the short name of the chaincode to deploy."
        echo "    -ccp <path>  - path to the chaincode."
        echo "    -ccv <version>  - Optional, chaincode version. 1.0 (default)"
        echo "    -ccs <sequence>  - Optional, chaincode definition sequence. Must be an integer, 1 (default), 2, 3, etc"
        echo "    -cci <fcn name>  - Optional, chaincode init required function to invoke. When provided this function will be invoked after deployment of the chaincode and will define the chaincode as initialization required."
        echo "    Used with "$'\e[0;32m'network.sh setOrg$'\e[0m':
        echo "    -o <organizaion name> - cn/ru/pk/iana"
        echo
        echo "    -h - print this message"
        echo
        echo " Examples:"
        echo "   ./network.sh up"
        echo "   ./network.sh createC -c local-root-chain"
        echo "   ./network.sh deployCC -c local-root-chain -ccn ProcedureCC -ccp ../../local-root-chain/chaincode/procedure-cc"
    echo "   . ./network.sh setOrg -o cn"
}

function networkUp() {
    ./set-network.sh
    # ./organizations/ccp-generate.sh
}

function networkDown() {
    ./delete-network.sh
}

function networkRestart() {
    networkDown
    networkUp
}

function createChannel() {
    if [ "$CHANNEL_NAME" == "" ]; then
        echo "Please use -c to input channel name"
        exit 1
    fi
    ./create-channel.sh $CHANNEL_NAME
}

function deployCC() {
    if [ $CHANNEL_NAME == "" ]; then
        echo "Please use -c to input channel name"
        exit 1
    fi
    if [ $CC_NAME == "" ]; then
        echo "Please use -ccn to input chaincode name"
        exit 1
    fi
        if [ $CC_SRC_PATH == "" ]; then
        echo "Please use -ccp to input chaincode path"
        exit 1
    fi
    ./deploy-chaincode.sh $CHANNEL_NAME $CC_NAME $CC_SRC_PATH $CC_VERSION $CC_SEQUENCE $CC_INIT_FCN
}

function setOrg() {
    if [ $ORG_NAME == "" ]; then
        echo "Please use -o to input organization name"
        exit 1
    fi
    setGlobals $ORG_NAME
}

# channel name
CHANNEL_NAME=""
# chaincode name
CC_NAME=""
# chaincode path
CC_SRC_PATH=""
# chaincode init function
CC_INIT_FCN="NA"
# Chaincode version
CC_VERSION="1.0"
# Chaincode definition sequence
CC_SEQUENCE=1
# Oragnization name
ORG_NAME=""

# Parse commandline args
## Parse mode
if [[ $# -lt 1 ]] ; then
        printHelp
        exit 0
else
        MODE=$1
        shift
fi
# parse flags
while [[ $# -ge 1 ]] ; do
        key="$1"
        case $key in
        -h )
          printHelp
          exit 0
          ;;
        -c )
          CHANNEL_NAME="$2"
          shift
          ;;
        -ccn )
          CC_NAME="$2"
          shift
          ;;
        -ccv )
          CC_VERSION="$2"
          shift
          ;;
        -ccs )
          CC_SEQUENCE="$2"
          shift
          ;;
        -ccp )
          CC_SRC_PATH="$2"
          shift
          ;;
        -cci )
          CC_INIT_FCN="$2"
          shift
          ;;
    -o )
          ORG_NAME="$2"
          shift
          ;;
        * )
          echo
          echo "Unknown flag: $key"
          echo
          printHelp
          exit 1
          ;;
        esac
        shift
done

# Determine mode of operation and printing out what we asked for
if [ "$MODE" == "up" ]; then
        networkUp
elif [ "$MODE" == "createC" ]; then
        createChannel
elif [ "$MODE" == "down" ]; then
        networkDown
elif [ "$MODE" == "restart" ]; then
        networkRestart
elif [ "$MODE" == "deployCC" ]; then
        deployCC
elif [ "$MODE" == "setOrg" ]; then
        setOrg
else
        printHelp
fi

