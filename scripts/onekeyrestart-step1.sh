# ./delete-network.sh
# ./delete-ca.sh
# ./set-org-ca.sh
# ./get-cert.sh
# ./set-network.sh
# ./org-node-start.sh
# ./create_channel.sh local-root-chain
# ./join-channel-org.sh local-root-chain
# ./install-approve-chaincode.sh
# ./commit-chaincode.sh
# ./ccp-generate.sh

# cd ../organizations

# orgs="cn ru iana"
# for org in $orgs
# do
#     cd $org
#     cp connection-${org}.yaml /home/localrootchain/service_proxy/gateway
#     cd ../
# done
all=false
while getopts "a" opt; do
    case "$opt" in
        h)
            printHelp
            exit 0
            ;;
        a)  all=true
            ;;
    esac
done
shift $[$OPTIND-1]


./delete-network.sh
if [ "$all" == "true" ]; then
    ./delete-ca.sh
    ./set-org-ca.sh
fi
