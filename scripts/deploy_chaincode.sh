#!/bin/bash

# $1:Channel name
# $2:Chaincode name
# $3:Chaincode path
# $4:Chaincode version
# $5:Chaincode sequence
# $6:Initial function name

function echoHelpInfo(){
	echo \# $1:Channel name
	echo \# $2:Chaincode name
	echo \# $3:Chaincode path
	echo \# $4:Chaincode version
	echo \# $5:Chaincode sequence
	echo \# $6:Initial function name

}

# check arguments exits

function vendorGoDependencies(){
	echo Vendoring Go dependencies at $CC_SRC_PATH
	pushd $CC_SRC_PATH
	GO111MODULE=on go mod vendor
	popd
	echo Finished vendoring Go dependencies
}



# import utils
. env-var.sh

cd ../chaincode-package

packageChaincode() {
    ORG=""
    for (( i = 0 ; i < ${#orgname[@]} ; i++ ))
    do
		if [ "${orgname[$i]}" == "orderer" ]; then
		continue
		fi
        ORG=${orgname[$i]}
        break
    done
	# ORG=$1
	setGlobals $ORG
	set -x
	peer lifecycle chaincode package ${CC_NAME}.tar.gz --path ${CC_SRC_PATH} --lang golang --label ${CC_NAME}_${CC_VERSION} >&log.txt
	res=$?
	set +x
	cat log.txt
}

# installChaincode PEER ORG
installChaincode() {
    for (( i = 0 ; i < ${#orgname[@]} ; i++ ))
	do
	if [ "${orgname[$i]}" == "orderer" ]; then
		continue
	fi
	# ORG=$1
    echo "Install chaincode on peer0.${orgname[$i]}..."
	setGlobals ${orgname[$i]}
	set -x
	peer lifecycle chaincode install ${CC_NAME}.tar.gz >&log.txt
	res=$?
	set +x
	cat log.txt
	done
}

# queryInstalled PEER ORG
queryInstalled() {
	#ORG=$1
    ORG=""
    for (( i = 0 ; i < ${#orgname[@]} ; i++ ))
    do
		if [ "${orgname[$i]}" == "orderer" ]; then
		continue
		fi
        ORG=${orgname[$i]}
        break
    done
	setGlobals $ORG
	set -x
	peer lifecycle chaincode queryinstalled >&log.txt
	res=$?
	set +x
	cat log.txt
	PACKAGE_ID=$(sed -n "/${CC_NAME}_${CC_VERSION}/{s/^Package ID: //; s/, Label:.*$//; p;}" log.txt)
	echo
}

# approveForMyOrg VERSION PEER ORG
approveForMyOrg() {
	# ORG=$1
    for (( i = 0 ; i < ${#orgname[@]} ; i++ ))
	do
	if [ "${orgname[$i]}" == "orderer" ]; then
		continue
	fi
	setGlobals ${orgname[$i]}
	set -x
	peer lifecycle chaincode approveformyorg -o ${org1IP}:11050 --ordererTLSHostnameOverride orderer0.orderer --tls --cafile $ORDERER0_CA --channelID $CHANNEL_NAME --name ${CC_NAME} --version ${CC_VERSION} --package-id ${PACKAGE_ID} --sequence ${CC_SEQUENCE} ${INIT_REQUIRED} ${CC_END_POLICY} ${CC_COLL_CONFIG} >&log.txt
	res=$?
	set +x
	cat log.txt
    done
}

# commitChaincodeDefinition VERSION PEER ORG (PEER ORG)...
commitChaincodeDefinition() {

	# while 'peer chaincode' command can get the orderer endpoint from the
	# peer (if join was successful), let's supply it directly as we know
	# it using the "-o" option
	ORG=""
    for (( i = 0 ; i < ${#orgname[@]} ; i++ ))
    do
		if [ "${orgname[$i]}" == "orderer" ]; then
		continue
		fi
        ORG=${orgname[$i]}
        break
    done
	setGlobals $ORG
	set -x
	peer lifecycle chaincode commit -o ${orgsIP[0]}:11050 --ordererTLSHostnameOverride orderer0.orderer --tls --cafile $ORDERER0_CA --channelID $CHANNEL_NAME --name ${CC_NAME} --peerAddresses ${orgsIP[1]}:7050 --tlsRootCertFiles ${ORGANIZATION_PATH}/${allOrg[1]}/crypto-config/peers/peer0.${allOrg[1]}/tls/ca.crt --peerAddresses ${orgsIP[2]}:8050 --tlsRootCertFiles ${ORGANIZATION_PATH}/${allOrg[2]}/crypto-config/peers/peer0.${allOrg[2]}/tls/ca.crt --peerAddresses ${orgsIP[3]}:9050 --tlsRootCertFiles ${ORGANIZATION_PATH}/${allOrg[3]}/crypto-config/peers/peer0.${allOrg[3]}/tls/ca.crt --peerAddresses ${orgsIP[4]}:10050 --tlsRootCertFiles ${ORGANIZATION_PATH}/${allOrg[4]}/crypto-config/peers/peer0.${allOrg[4]}/tls/ca.crt --version ${CC_VERSION} --sequence ${CC_SEQUENCE} ${INIT_REQUIRED} ${CC_END_POLICY} ${CC_COLL_CONFIG} >&log.txt
	# peer lifecycle chaincode commit -o ${orgsIP[0]}:11050 --ordererTLSHostnameOverride orderer0.orderer --tls --cafile $ORDERER0_CA --channelID $CHANNEL_NAME --name ${CC_NAME} --peerAddresses ${orgsIP[1]}:7050 --tlsRootCertFiles ${ORGANIZATION_PATH}/${allOrg[1]}/crypto-config/peers/peer0.${allOrg[1]}/tls/ca.crt --peerAddresses ${orgsIP[2]}:8050 --tlsRootCertFiles ${ORGANIZATION_PATH}/${allOrg[2]}/crypto-config/peers/peer0.${allOrg[2]}/tls/ca.crt --peerAddresses ${orgsIP[3]}:9050 --tlsRootCertFiles ${ORGANIZATION_PATH}/${allOrg[3]}/crypto-config/peers/peer0.${allOrg[3]}/tls/ca.crt --version ${CC_VERSION} --sequence ${CC_SEQUENCE} ${INIT_REQUIRED} ${CC_END_POLICY} ${CC_COLL_CONFIG} >&log.txt
	res=$?
	set +x
	cat log.txt
}

chaincodeInvokeInit() {
	#parsePeerConnectionParameters $@
	#res=$?
	#verifyResult $res "Invoke transaction failed on channel '$CHANNEL_NAME' due to uneven number of peer and org parameters "

	# while 'peer chaincode' command can get the orderer endpoint from the
	# peer (if join was successful), let's supply it directly as we know
	# it using the "-o" option
	set -x
	fcn_call='{"function":"'${CC_INIT_FCN}'","Args":[]}'
	echo invoke fcn call:${fcn_call}
	peer chaincode invoke -o localhost:11050 --ordererTLSHostnameOverride orderer0.orderer --tls --cafile $ORDERER0_CA -C $CHANNEL_NAME -n ${CC_NAME} --peerAddresses localhost:7050 --tlsRootCertFiles /home/lrc-deploy/network/cn/crypto-config/peers/peer0.cn/tls/ca.crt --peerAddresses localhost:8050 --tlsRootCertFiles /home/lrc-deploy/network/ru/crypto-config/peers/peer0.ru/tls/ca.crt --peerAddresses localhost:10050 --tlsRootCertFiles /home/lrc-deploy/network/pk/crypto-config/peers/peer0.pk/tls/ca.crt --peerAddresses localhost:9050 --tlsRootCertFiles /home/lrc-deploy/network/iana/crypto-config/peers/peer0.iana/tls/ca.crt --isInit -c ${fcn_call} >&log.txt
	res=$?
	set +x
	cat log.txt
}

chaincodeQuery() {
	ORG=$1
	setGlobals $ORG
	echo "===================== Querying on peer0.${ORG} on channel '$CHANNEL_NAME'... ===================== "
	echo "CORE_PEER_LOCALMSPID=$CORE_PEER_LOCALMSPID"
	echo "FABRIC_CFG_PATH=$FABRIC_CFG_PATH"
	local rc=1
	local COUNTER=1
	# continue to poll
	# we either get a successful response, or reach MAX RETRY
	while [ $rc -ne 0 -a $COUNTER -lt 5 ]; do
		sleep 3
		echo "Attempting to Query peer0.${ORG}, Retry after 3 seconds."
		set -x
		peer chaincode query -C $CHANNEL_NAME -n ${CC_NAME} -c '{"Args":["queryAllCars"]}' >&log.txt
		res=$?
		set +x
		let rc=$res
		COUNTER=$(expr $COUNTER + 1)
	done
	echo
	cat log.txt
	if test $rc -eq 0; then
		echo "===================== Query successful on peer0.${ORG} on channel '$CHANNEL_NAME' ===================== "
		echo
	else
		echo
		echo $'\e[1;31m'"!!!!!!!!!!!!!!! After $MAX_RETRY attempts, Query result on peer0.${ORG} is INVALID !!!!!!!!!!!!!!!!"$'\e[0m'
		echo
		exit 1
	fi
}

package=false
install=false
query=false
aprrove=false
commit=false

while getopts "piqac" opt; do
    case "$opt" in
        h)
            printHelp
            exit 0
            ;;
        p)  package=true
            ;;
        i)  install=true
            ;;
        q)  query=true
            ;;
        a)  approve=true
            ;;
        c)  commit=true
            ;;
    esac
done
shift $[$OPTIND-1]

export PATH=${PWD}/../../bin:$PATH
export CHANNEL_NAME=$1
export FABRIC_CFG_PATH=${PWD}/../config/
export GOPROXY=https://goproxy.cn

# Change here to deploy another chaincode
export CC_NAME=$2
export CC_SRC_PATH=$3
export CC_VERSION=$4
export CC_SEQUENCE=$5

vendorGoDependencies

CC_INIT_FCN=${6:-"NA"}
CC_END_POLICY="NA"
CC_COLL_CONFIG="NA"
INIT_REQUIRED="--init-required"

# check if the init fcn should be called
if [ "$CC_INIT_FCN" = "NA" ]; then
	INIT_REQUIRED=""
fi

if [ "$CC_END_POLICY" = "NA" ]; then
	CC_END_POLICY=""
else
	CC_END_POLICY="--signature-policy $CC_END_POLICY"
fi

if [ "$CC_COLL_CONFIG" = "NA" ]; then
	CC_COLL_CONFIG=""
else
	CC_COLL_CONFIG="--collections-config $CC_COLL_CONFIG"
fi



if [ "$package" == "true" ]; then
    echo
    echo "Clone hyperledger/fabric-samples repo"
    echo
    packageChaincode
fi
if [ "$install" == "true" ]; then
    echo
    echo "Pull Hyperledger Fabric binaries"
    echo
    installChaincode
fi
if [ "$query" == "true" ]; then
    echo
    echo "Pull Hyperledger Fabric docker images"
    echo
    queryInstalled
fi
if [ "$approve" == "true" ]; then
    echo
    echo "Clone hyperledger/fabric-samples repo"
    echo
    approveForMyOrg
fi
if [ "$commit" == "true" ]; then
    echo
    echo "Pull Hyperledger Fabric binaries"
    echo
    commitChaincodeDefinition
fi




## package the chaincode
# packageChaincode cn

## Install chaincode on peer0.org1 and peer0.org2
# echo "Install chaincode on peer0.cn..."
# installChaincode cn
# echo "Install chaincode on peer0.ru..."
# installChaincode ru
# echo "Install chaincode on peer0.pk..."
# installChaincode pk
# echo "Install chaincode on peer0.iana..."
# installChaincode iana

## query whether the chaincode is installed
# queryInstalled cn

## approve the definition for cn
# approveForMyOrg cn

## now approve for ru
# approveForMyOrg ru

## now approve for pk
# approveForMyOrg pk

## now approve for iana
# approveForMyOrg iana

## now that we know for sure all orgs have approved, commit the definition
# sleep 3
# commitChaincodeDefinition

## Invoke the chaincode - this does require that the chaincode have the 'initLedger'
## method defined
if [ "$CC_INIT_FCN" = "NA" ]; then
	echo "===================== Chaincode initialization is not required ===================== "
	echo
else
	chaincodeInvokeInit
fi
# chaincodeQuery cn
# chaincodeQuery ru
# chaincodeQuery pk
# chaincodeQuery iana

exit 0
