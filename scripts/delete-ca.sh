#!/bin/bash
#
. env-var.sh
cd ../organizations

# orgs="cn ru iana orderer"
for (( i = 0 ; i < ${#allOrg[@]} ; i++ ))
do
    cd ${allOrg[$i]}
    docker-compose -f docker-compose-ca.yaml down
    rm -r ./ca-home/*
    rm -r ./crypto-config/*
    cd ../
done


