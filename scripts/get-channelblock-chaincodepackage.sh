export PATH=${PWD}/../../bin:$PATH
export FABRIC_CFG_PATH=${PWD}/../config/
export CHANNEL_NAME=$1

. env-var.sh


fetchChannel() {
    ORG=""
    for (( i = 0 ; i < ${#orgname[@]} ; i++ ))
    do
		if [ "${orgname[$i]}" == "orderer" ]; then
		continue
		fi
        ORG=${orgname[$i]}
        break
    done
	# ORG=$1
	setGlobals $ORG
    local rc=1
    local COUNTER=1
        ## Sometimes Join takes time, hence retry
    while [ $rc -ne 0 -a $COUNTER -lt $MAX_RETRY ] ; do
        sleep $DELAY
        set -x
        peer channel fetch oldest -c local-root-chain ./../channel-artifacts/local-root-chain.block --orderer ${org1IP}:11050 --tls --cafile $ORDERER0_CA
        # peer channel fetch newest -c local-root-chain  --orderer ${org1IP}:11050 --tls --cafile $ORDERER0_CA
        res=$?
        set +x
        let rc=$res
        COUNTER=$(expr $COUNTER + 1)
    done
    echo
    verifyResult $res "After $MAX_RETRY attempts, peer0.org${ORG} has failed to fetch channel '$CHANNEL_NAME' "
}

queryInstalled() {
        #ORG=$1
    CC_NAME=$1
    CC_VERSION=$2
    ORG=""
    for (( i = 0 ; i < ${#allOrg[@]} ; i++ ))
    do
        if [ "${allOrg[$i]}" == "orderer" ]; then
		continue
	    fi
        if [[ "${orgname[@]}"  =~ "${allOrg[$i]}" ]]; then
            # echo "jump"
            continue
        fi
        ORG=${allOrg[$i]}
        break
    done
    setGlobals $ORG
    set -x
    peer lifecycle chaincode queryinstalled >&log.txt
    res=$?
    set +x
    cat log.txt
    PACKAGE_ID=$(sed -n "/${CC_NAME}_${CC_VERSION}/{s/^Package ID: //; s/, Label:.*$//; p;}" log.txt)
    echo
}

listChannel() {
    ORG=$1
    setGlobals $ORG
    local rc=1
    local COUNTER=1
    ## Sometimes Join takes time, hence retry
    while [ $rc -ne 0 -a $COUNTER -lt $MAX_RETRY ] ; do
        sleep $DELAY
        set -x
        peer channel list
        res=$?
        set +x
        let rc=$res
        COUNTER=$(expr $COUNTER + 1)
    done

    echo
    verifyResult $res "After $MAX_RETRY attempts, peer0.org${ORG} has failed to join channel '$CHANNEL_NAME' "
}

getInstalledPackage(){
    ORG=""
    i=0
    for (( i = 0 ; i < ${#allOrg[@]} ; i++ ))
    do
        if [ "${allOrg[$i]}" == "orderer" ]; then
		continue
	    fi
        if [[ "${orgname[@]}"  =~ "${allOrg[$i]}" ]]; then
            # echo "jump"
            continue
        fi
        ORG=${allOrg[$i]}
        break
    done
    setGlobals $ORG
    set -x
    peer lifecycle chaincode getinstalledpackage --package-id ${PACKAGE_ID} --output-directory ./../chaincode-package/ --peerAddresses ${orgsIP[$i]}:7050 --tlsRootCertFiles ./../organizations/${allOrg[$i]}/crypto-config/peers/peer0.${allOrg[$i]}/tls/ca.crt
    # peer lifecycle chaincode getinstalledpackage --package-id ${PACKAGE_ID} --output-directory ./../scripts/ --peerAddresses ${orgsIP[$i]}:7050 --tlsRootCertFiles ./../organizations/${allOrg[$i]}/crypto-config/peers/peer0.${allOrg[$i]}/tls/ca.crt
    res=$?
    set +x


}

verifyResult() {
  if [ $1 -ne 0 ]; then
    echo "!!!!!!!!!!!!!!! "$2" !!!!!!!!!!!!!!!!"
    echo
    exit 1
  fi
}

package=false
channelblock=false

while getopts "pc" opt; do
    case "$opt" in
        h)
            printHelp
            exit 0
            ;;
        p)  package=true
            ;;
        c)  channelblock=true
            ;;
    esac
done
shift $[$OPTIND-1]

if [ "$channelblock" == "true" ]; then
    echo
    echo "Fetch channel block"
    echo
    fetchChannel
fi

if [ "$package" == "true" ]; then
    echo
    echo "Get chaincode package"
    echo
    queryInstalled  LocalRootMembershipCC 1.0
    getInstalledPackage
    mv ./../chaincode-package/LocalRootMembershipCC* ./../chaincode-package/LocalRootMembershipCC.tar.gz

    queryInstalled  LocalRootServiceCC 1.0
    getInstalledPackage
    mv ./../chaincode-package/LocalRootServiceCC* ./../chaincode-package/LocalRootServiceCC.tar.gz

    queryInstalled  TLDDelegationCC 1.0
    getInstalledPackage
    mv ./../chaincode-package/TLDDelegationCC* ./../chaincode-package/TLDDelegationCC.tar.gz
fi



#queryInstalled
# listChannel hit
