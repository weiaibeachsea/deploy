#!/bin/bash
#

export DELAY=3
export MAX_RETRY=3

export ORGANIZATION_PATH=${PWD}/../organizations

export CORE_PEER_TLS_ENABLED=true
export ORDERER0_CA=${ORGANIZATION_PATH}/orderer/crypto-config/orderers/orderer0.orderer/msp/tlscacerts/tlsca.orderer-cert.pem
export PEER0_HIT_CA=${ORGANIZATION_PATH}/hit/crypto-config/peers/peer0.hit/tls/ca.crt
export PEER0_PCNL_CA=${ORGANIZATION_PATH}/pcnl/crypto-config/peers/peer0.pcnl/tls/ca.crt
export PEER0_NCSE_CA=${ORGANIZATION_PATH}/ncse/crypto-config/peers/peer0.ncse/tls/ca.crt
export PEER0_IANA_CA=${ORGANIZATION_PATH}/iana/crypto-config/peers/peer0.iana/tls/ca.crt

export allOrg=("orderer" "hit" "pcnl" "iana" "ncse")
# export allOrg=("orderer" "hit" "pcnl" "iana")

export org1IP="121.36.75.190"
export org2IP="121.36.75.190"
export org3IP="121.36.75.190"
export org4IP="121.36.75.190"
export org5IP="121.36.75.190"
export projectPath="/home/localrootchain/deploy/"
export orgsIP=($org1IP $org2IP $org3IP $org4IP $org5IP)
# export orgsIP=($org1IP $org2IP $org3IP $org4IP)

export orgname=("orderer" "hit" "pcnl" "iana" "ncse")

# Set OrdererOrg.admin globals
setOrdererGlobals() {
  export CORE_PEER_LOCALMSPID="ordererMSP"
  export CORE_PEER_TLS_ROOTCERT_FILE=${ORGANIZATION_PATH}/orderer/crypto-config/orderers/orderer0.orderer/msp/tlscacerts/tlsca.orderer-cert.pem
  export CORE_PEER_MSPCONFIGPATH=${ORGANIZATION_PATH}/orderer/crypto-config/users/admin@orderer/msp
}

# Set environment variables for the peer org
setGlobals() {
  USING_ORG=$1
  echo "Using organization ${USING_ORG}"
  if [ $USING_ORG == "hit" ]; then
    export CORE_PEER_LOCALMSPID="hitMSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_HIT_CA
    export CORE_PEER_MSPCONFIGPATH=${ORGANIZATION_PATH}/hit/crypto-config/users/admin@hit/msp
    export CORE_PEER_ADDRESS=${org2IP}:7050
  elif [ $USING_ORG == "pcnl" ]; then
    export CORE_PEER_LOCALMSPID="pcnlMSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_PCNL_CA
    export CORE_PEER_MSPCONFIGPATH=${ORGANIZATION_PATH}/pcnl/crypto-config/users/admin@pcnl/msp
    export CORE_PEER_ADDRESS=${org3IP}:8050
  elif [ $USING_ORG == "iana" ]; then
    export CORE_PEER_LOCALMSPID="ianaMSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_IANA_CA
    export CORE_PEER_MSPCONFIGPATH=${ORGANIZATION_PATH}/iana/crypto-config/users/admin@iana/msp
    export CORE_PEER_ADDRESS=${org4IP}:9050    
  elif [ $USING_ORG == "ncse" ]; then
    export CORE_PEER_LOCALMSPID="ncseMSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_NCSE_CA
    export CORE_PEER_MSPCONFIGPATH=${ORGANIZATION_PATH}/ncse/crypto-config/users/admin@ncse/msp
    export CORE_PEER_ADDRESS=${org5IP}:10050
  else
    echo "================== ERROR !!! ORG Unknown =================="
  fi
}
