#!/bin/bash
#

cd ..

export PATH=${PWD}/../bin:$PATH
export FABRIC_CFG_PATH=${PWD}/

# create consortium
set -x
configtxgen -profile LocalRootChainOrdererGenesis -channelID system-channel -outputBlock ./system-genesis-block/genesis.block
res=$?
set +x
if [ $res -ne 0 ]; then
echo $'\e[1;32m'"Failed to generate orderer genesis block..."$'\e[0m'
exit 1
fi

# cd organizations

# orgs="orderer"
# for org in $orgs
# do
#     cd $org
#     docker-compose up -d
#     cd ../
# done

# docker ps -a
