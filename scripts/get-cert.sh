#!/bin/bash
#
. env-var.sh
cd ../organizations

# orgs="cn ru iana orderer"

for (( i = 0 ; i < ${#allOrg[@]} ; i++ ))
do
    cd ${allOrg[$i]}
    ./register-enroll.sh
    cd ../
done

