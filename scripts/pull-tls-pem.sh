. env-var.sh

for (( i = 0 ; i < ${#allOrg[@]} ; i++ ))
do
    if [[ "${orgname[@]}"  =~ "${allOrg[$i]}" ]]; then
    echo "jump"
    continue
    fi
  set -x
  scp ${orgsIP[$i]}:${projectPath}/organizations/${allOrg[$i]}/ca-home/tls-cert.pem ${projectPath}/organizations/${allOrg[$i]}/ca-home/tls-cert.pem
  set +x
done
