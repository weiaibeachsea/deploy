all=false
while getopts "a" opt; do
    case "$opt" in
        h)
            printHelp
            exit 0
            ;;
        a)  all=true
            ;;
    esac
done
shift $[$OPTIND-1]

if [ "$all" == "true" ]; then
    ./pull-tls-pem.sh
    ./get-cert.sh
    ./org-node-start.sh
    ./get-channelblock-chaincodepackage.sh -cp
    ./join-channel-org.sh local-root-chain
    ./install-approve-chaincode.sh
    exit 0
fi

# ./pull-tls-pem.sh
# ./get-cert.sh
./org-node-start.sh
./get-channelblock-chaincodepackage.sh -cp
./join-channel-org.sh local-root-chain
./install-approve-chaincode.sh
