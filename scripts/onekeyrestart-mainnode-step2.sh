all=false
while getopts "a" opt; do
    case "$opt" in
        h)
            printHelp
            exit 0
            ;;
        a)  all=true
            ;;
    esac
done
shift $[$OPTIND-1]

if [ "$all" == "true" ]; then
    ./pull-tls-pem.sh
    ./get-cert.sh
    # ./push-crypto-config.sh
    ./set-network.sh
    ./org-node-start.sh
    ./create_channel.sh local-root-chain
    # ./push-channel-block.sh
    ./join-channel-org.sh local-root-chain
    ./pack-chaincode.sh
    ./install-approve-chaincode.sh
    exit 0
fi
# ./pull-tls-pem.sh
# ./get-cert.sh
# ./push-crypto-config.sh
./set-network.sh
./org-node-start.sh
./create_channel.sh local-root-chain
# ./push-channel-block.sh
./join-channel-org.sh local-root-chain
./pack-chaincode.sh
./install-approve-chaincode.sh
