#!/bin/bash

function one_line_pem {
    echo "`awk 'NF {sub(/\\n/, ""); printf "%s\\\\\\\n",$0;}' $1`"
}

function json_ccp {
    local PP=$(one_line_pem $4)
    local CP=$(one_line_pem $5)
    sed -e "s/\${ORG}/$1/" \
        -e "s/\${P0PORT}/$2/" \
        -e "s/\${CAPORT}/$3/" \
        -e "s#\${PEERPEM}#$PP#" \
        -e "s#\${CAPEM}#$CP#" \
        -e "s/\${IP}/$6/" \
        ../scripts/ccp-template.json
}

function yaml_ccp {
    local PP=$(one_line_pem $4)
    local CP=$(one_line_pem $5)
    sed -e "s/\${ORG}/$1/" \
        -e "s/\${P0PORT}/$2/" \
        -e "s/\${CAPORT}/$3/" \
        -e "s#\${PEERPEM}#$PP#" \
        -e "s#\${CAPEM}#$CP#" \
        -e "s/\${IP}/$6/" \
        ../scripts/ccp-template.yaml | sed -e $'s/\\\\n/\\\n          /g'
}
. env-var.sh
cd ../organizations

ORG=hit
P0PORT=7050
CAPORT=7054
PEERPEM=hit/crypto-config/tlsca/tlsca.hit-cert.pem
CAPEM=hit/crypto-config/ca/ca.hit-cert.pem
echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $org2IP)" > hit/connection-hit.json
echo "$(yaml_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $org2IP)" > hit/connection-hit.yaml

ORG=pcnl
P0PORT=8050
CAPORT=8054
PEERPEM=pcnl/crypto-config/tlsca/tlsca.pcnl-cert.pem
CAPEM=pcnl/crypto-config/ca/ca.pcnl-cert.pem
echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $org3IP)" > pcnl/connection-pcnl.json
echo "$(yaml_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $org3IP)" > pcnl/connection-pcnl.yaml

ORG=iana
P0PORT=9050
CAPORT=9054
PEERPEM=iana/crypto-config/tlsca/tlsca.iana-cert.pem
CAPEM=iana/crypto-config/ca/ca.iana-cert.pem
echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $org4IP)" > iana/connection-iana.json
echo "$(yaml_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $org4IP)" > iana/connection-iana.yaml

ORG=ncse
P0PORT=10050
CAPORT=10054
PEERPEM=ncse/crypto-config/tlsca/tlsca.ncse-cert.pem
CAPEM=ncse/crypto-config/ca/ca.ncse-cert.pem
echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $org5IP)" > ncse/connection-ncse.json
echo "$(yaml_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $org5IP)" > ncse/connection-ncse.yaml