#!/bin/bash
#
. env-var.sh
cd ../organizations

# orgs=${orgname}
for (( i = 0 ; i < ${#orgname[@]} ; i++ ))
do
    cd ${orgname[$i]}
    cp fabric-ca-server-config.yaml ca-home
    docker-compose -f docker-compose-ca.yaml up -d
    cd ../
done


