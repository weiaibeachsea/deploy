#!/bin/bash
#
. env-var.sh
cd ../organizations

# orgs="orderer cn ru iana"

for (( i = 0 ; i < ${#allOrg[@]} ; i++ ))
do
    cd ${allOrg[$i]}
    docker-compose down --volumes
    cd ../
done

docker rm $(docker ps -a | grep "hyperledger/*" | awk "{print \$1}")
docker rm -f $(docker ps -a | awk '($2 ~ /dev-peer.*/) {print $1}')
docker rmi -f $(docker images | awk '($1 ~ /dev-peer.*/) {print $3}')
docker volume prune -f

rm -r ../channel-artifacts/*
rm -r ../system-genesis-block/*
rm -r ../chaincode-package/*
docker ps -a
