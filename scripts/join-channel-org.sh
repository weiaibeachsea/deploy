#!/bin/bash
#

function printHelp() {
	echo "Usage: "
	echo "  create-channel.sh <channel_name>"
}

if [ $# -ne 1 ] ; then
  printHelp
fi

export PATH=${PWD}/../../bin:$PATH
export FABRIC_CFG_PATH=${PWD}/../
export CHANNEL_NAME=$1

. env-var.sh

createChannelTx() {

	set -x
	configtxgen -profile LocalRootChainChannel -outputCreateChannelTx ../channel-artifacts/${CHANNEL_NAME}.tx -channelID $CHANNEL_NAME
	res=$?
	set +x
	if [ $res -ne 0 ]; then
		echo "Failed to generate channel configuration transaction..."
		exit 1
	fi
	echo

}

createAncorPeerTx() {

	# for orgmsp in ${orgname}; do
	for (( i = 0 ; i < ${#orgname[@]} ; i++ ))
	do
	if [ "${orgname[$i]}" == "orderer" ]; then
		continue
	fi
	echo "#######    Generating anchor peer update transaction for ${orgname[$i]}MSP  ##########"
	set -x
	configtxgen -profile LocalRootChainChannel -outputAnchorPeersUpdate ../channel-artifacts/${orgname[$i]}MSPanchors.tx -channelID $CHANNEL_NAME -asOrg ${orgname[$i]}MSP
	res=$?
	set +x
	if [ $res -ne 0 ]; then
		echo "Failed to generate anchor peer update transaction for ${orgname[$i]}MSP..."
		exit 1
	fi
	echo
	done
}

createChannel() {
	setGlobals cn
	# Poll in case the raft leader is not set yet
	local rc=1
	local COUNTER=1
	while [ $rc -ne 0 -a $COUNTER -lt $MAX_RETRY ] ; do
		sleep $DELAY
		set -x
		peer channel create -o localhost:11050 -c $CHANNEL_NAME --ordererTLSHostnameOverride orderer0.orderer -f ../channel-artifacts/${CHANNEL_NAME}.tx --outputBlock ../channel-artifacts/${CHANNEL_NAME}.block --tls --cafile $ORDERER0_CA >&log.txt
		res=$?
		set +x
		let rc=$res
		COUNTER=$(expr $COUNTER + 1)
	done
	cat log.txt
	verifyResult $res "Channel creation failed"
	echo
	echo "===================== Channel '$CHANNEL_NAME' created ===================== "
	echo
}

# queryCommitted ORG
joinChannel() {
    for (( i = 0 ; i < ${#orgname[@]} ; i++ ))
	do
	if [ "${orgname[$i]}" == "orderer" ]; then
		continue
	fi
    setGlobals ${orgname[$i]}
	local rc=1
	local COUNTER=1
	## Sometimes Join takes time, hence retry
	while [ $rc -ne 0 -a $COUNTER -lt $MAX_RETRY ] ; do
    sleep $DELAY
    set -x
    peer channel join -b ../channel-artifacts/$CHANNEL_NAME.block >&log.txt
    res=$?
    set +x
		let rc=$res
		COUNTER=$(expr $COUNTER + 1)
	done
	cat log.txt
	echo
	verifyResult $res "After $MAX_RETRY attempts, peer0.org${orgname[$i]} has failed to join channel '$CHANNEL_NAME' "
    done
}

updateAnchorPeers() {
    for (( i = 0 ; i < ${#orgname[@]} ; i++ ))
	do
	if [ "${orgname[$i]}" == "orderer" ]; then
		continue
	fi
    setGlobals ${orgname[$i]}
	local rc=1
	local COUNTER=1
	## Sometimes Join takes time, hence retry
	while [ $rc -ne 0 -a $COUNTER -lt $MAX_RETRY ] ; do
    sleep $DELAY
    set -x
		peer channel update -o ${org1IP}:11050 --ordererTLSHostnameOverride orderer0.orderer -c $CHANNEL_NAME -f ../channel-artifacts/${CORE_PEER_LOCALMSPID}anchors.tx --tls --cafile $ORDERER0_CA >&log.txt
    res=$?
    set +x
		let rc=$res
		COUNTER=$(expr $COUNTER + 1)
	done
	cat log.txt
  verifyResult $res "Anchor peer update failed"
  echo "===================== Anchor peers updated for org '$CORE_PEER_LOCALMSPID' on channel '$CHANNEL_NAME' ===================== "
  sleep $DELAY
  echo
  done
}

verifyResult() {
  if [ $1 -ne 0 ]; then
    echo "!!!!!!!!!!!!!!! "$2" !!!!!!!!!!!!!!!!"
    echo
    exit 1
  fi
}

# FABRIC_CFG_PATH=${PWD}

cd ../organizations

## Create channeltx
# echo "### Generating channel create transaction '${CHANNEL_NAME}.tx' ###"
# createChannelTx

## Create anchorpeertx
echo "### Generating anchor peer update transactions ###"
createAncorPeerTx

FABRIC_CFG_PATH=${PWD}/../config

## Create channel
# echo "Creating channel "$CHANNEL_NAME
# createChannel

## Join all the peers to the channel
echo "Join peers to the channel..."
joinChannel # cn
# joinChannel ru
# joinChannel iana
# joinChannel pk

## Set the anchor peers for each org in the channel
echo "Updating anchor peers..."
updateAnchorPeers # cn
# updateAnchorPeers ru
# updateAnchorPeers pk
# updateAnchorPeers iana

echo
echo "========= Channel successfully joined =========== "
echo

exit 0
